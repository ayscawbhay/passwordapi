﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

namespace PasswordApiServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PwdController : ControllerBase
    {
        private static PwdSizes _sizes;

        [HttpGet]
        public JsonResult Get(string jsonsizes)
        {
            PwdGenerator password;
            _sizes = JsonConvert.DeserializeObject<PwdSizes>(jsonsizes);

            try
            {
                if (_sizes.PasswordLenght > 8 || _sizes.PasswordLenght < 4 || _sizes.PasswordLenght % 2 != 0)
                {
                    throw new Exception("Длина числового кода должна быть 4-8 символов (кратно 2-м)");
                }
                if (_sizes.SaltLength < 8)
                {
                    throw new Exception("Минимальная длина соли 8 символов");
                }
            }
            catch (Exception e)
            {
                _sizes = null;
                return new JsonResult(e.Message);
            }

            password = new PwdGenerator(_sizes.PasswordLenght, _sizes.SaltLength);

            var result = JsonConvert.SerializeObject(password);

            return new JsonResult(result);
        }

    }
}
