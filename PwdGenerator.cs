﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
    using System.Security.Cryptography;

namespace PasswordApiServer
{
    public class PwdGenerator
    {
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("salt")]
        public string Salt { get; set; }
        [JsonProperty("hash")]
        public string HashSHA1 { get; set; }

        private const int _iterationsCount = 64000;

        public PwdGenerator(int pwdLength, int saltLength)
        {
            Password = CreatePassword(pwdLength);
            Salt = GenerateSalt(saltLength);
            HashSHA1 = ComputeHash(Password, Salt);
        }

        private string CreatePassword(int length)
        {
            var password = new int[length];
            //using (var provider = new RNGCryptoServiceProvider())
            //    provider.GetBytes(password);
            for (int i = 0; i < length; i++)
                password[i] = RandomNumberGenerator.GetInt32(1, 10);
            return String.Join("", password);
        }

        private string GenerateSalt(int length)
        {
            var salt = new byte[length];
            using (var provider = new RNGCryptoServiceProvider())
                provider.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        private string ComputeHash(string password, string salt)
        {
            byte[] derived;
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, Convert.FromBase64String(salt), 
                _iterationsCount, HashAlgorithmName.SHA1))
            {
                derived = pbkdf2.GetBytes(32);
            }
            return Convert.ToBase64String(derived);
        }
    }
}