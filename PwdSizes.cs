﻿using Newtonsoft.Json;

namespace PasswordApiServer
{
    public class PwdSizes
    {
        [JsonProperty("pwdLength")]
        public int PasswordLenght { get; set; }
        [JsonProperty("saltLength")]
        public int SaltLength { get; set; }
    }
}
